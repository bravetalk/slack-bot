FROM alpine:latest

RUN apk update \
	&& apk add --no-cache python3 py3-pip python3-dev openssl-dev

WORKDIR /opt/slackbot
COPY genslug.py bot.py requirements.txt ./

RUN pip3 install --pre -r requirements.txt

CMD ["python3","bot.py"]