import rstr
import random
import requests

length = 42
base_url = "https://talk.brave.com/"

def generate_regex():
	rint = random.randint(1,length+1)
	regex = rstr.xeger(r'[a-zA-Z0-9-]{%d}_[a-zA-Z0-9-]{%d}' % (rint,length-rint))
	
	return regex

def check_url():	
	current_regex = generate_regex()
	url = f"{base_url}{current_regex}"

	return url