<p align="center"></p>
<br>
<h1 align="center">BraveTalk - Unofficial Slack Bot</h1>

<p align="center">Python based slack application/bot build from slack_blot for generating talk.brave.com meeting link using regex format.</p>
<br>
<p align="center">
  <img src="https://img.shields.io/badge/python-v3.7-blue" alt="Python V3.7" />
  <img src="https://app.buddy.works/selfhosted/slack-bot/pipelines/pipeline/376920/badge.svg?token=5568c0e8a4a62261b4532253e932c38eb59baf20e245617fda43f06748766e1f" alt="build status" />
  <img src="https://img.shields.io/badge/analyze-passed-rightgreen" alt="Analyze" />
  <img src="https://img.shields.io/badge/dependencies-up%20to%20date-brightgreen" alt="Test" />
  <img src="https://img.shields.io/badge/license-GNU GPLv3.0-green" alt="License" />
  <img src="https://img.shields.io/badge/Status-up-brightgreen" alt="status-up" />
  <img src="https://img.shields.io/badge/issues-open-yellow" alt="Issues Open" />
  <img src="https://sonarcloud.io/api/project_badges/measure?project=bravetalk_slack-bot&metric=alert_status" alt="sonarcloud" />
  <img src="https://sonarcloud.io/api/project_badges/measure?project=bravetalk_slack-bot&metric=vulnerabilities" alt="vulnerability" />
</p>
<br>

### Prerequisites
1. A Slack account. If you don’t already have one, you can create one here.
2. Python 3.6 or newer. If you do not have python installed on your computer, you can download it here.

## Slack Setup (src: [twilio](https://www.twilio.com/blog/how-to-build-a-slackbot-in-socket-mode-with-python))
### Create the Slackbot

Now that you have set up the basic configuration for your project, it's time to create the Slackbot.

If you would like to build your bot in a new workspace, [create one here](https://slack.com/get-started#/create). If you are already part of a workspace where you would like to build your bot, keep reading!

To start, navigate to [the Slack apps dashboard for the Slack API](https://api.slack.com/apps). Here, you should find a green button that says **Create New App**. When you click this button, select the option to create your app _from scratch._

Create a name for your bot, such as "Simple-Bot". Select the workspace you would like your bot to exist in. If your workspace has app restrictions, you will see a yellow warning message similar to the one shown below. This is okay! Click **Create App**.

![create a name for the slackbot and pick a workspace](https://twilio-cms-prod.s3.amazonaws.com/images/GUgZzr5G7eg7we6eg9MejkDvvw_ghmX2WsYPpqGNmO7gz1.width-800.png)

### Configure the Slackbot

Once you have created your bot, you should see a page like the following.

![screenshot of the basic information for the simple-bot slack bot](https://twilio-cms-prod.s3.amazonaws.com/images/ldIVfbTpSdOrgUwS-ujTczfrIMW-f3Q-Y2PgwwOZnyQS34.width-800.png)

Here, we will need to configure some settings so that the bot behaves properly. Navigate to the “OAuth & Permissions” tab under _Features_ on the left panel.

Scroll down, and add the following scopes under _Bot Token Scopes_:

*   **app\_mentions.read:** allows the bot to view messages that mention your bot.
*   **channels:join:** allows the bot to join channels.
*   **chat:write**: allows the bot to post messages in a public channel.

![list of scopes required for the slackbot](https://twilio-cms-prod.s3.amazonaws.com/images/sE_tUp7pYC5pf0dj8rD76jK-Ldujmt3Fq7xvJesoTecOxP.width-800.png)

#### Enable Socket Mode for the Slackbot

Next, we will enable Socket Mode. Socket Mode allows our bot to interact in our workspace without exposing a public HTTP endpoint, which might not be allowed behind a corporate firewall.

To enable Socket Mode, navigate to the “Socket Mode” tab under _Settings_ in the left panel. Toggle the button next to _Enable Socket Mode_.

Create an app level token for your bot as shown below. An app level token is connected to your app universally and across each organization that your app is used in. This is different from a bot level token, which will be discussed later. The token name is "simple\_bot\_app\_token" in this example.

![generate an app-level token to enable socket mode](https://twilio-cms-prod.s3.amazonaws.com/images/rqVX8rFLFnsY-H0XVNMMBYkvNqddqzAf8MByh2-SaOJlK1.width-800.png)

After clicking **Generate** you will receive an app level token which will be referred to as the `SLACK_APP_TOKEN`. Copy this token and store it somewhere safe - we will need it later. Click the **Done** button and make sure Socket Mode is enabled.

![toggle for socket mode enabled](https://twilio-cms-prod.s3.amazonaws.com/images/I-eSIUi7IUzUWbt8CPNUyv8c9vnKI2e_6a83VMoGobxNhJ.width-800.png)

#### Enable Event Subscriptions for the Slackbot

Once we have enabled Socket Mode, we will enable [Event Subscriptions](https://api.slack.com/apis/connections/events-api). Event subscriptions allow our bot to automatically respond to certain events, such as a mention or direct message. Navigate to “Event Subscriptions” under _Features_ in the left panel. Enable events.

![toggle for slack events enabled](https://twilio-cms-prod.s3.amazonaws.com/images/bkZ_cT0yZhJWszMSfLhxpgN0ueQq2wC-1jUOOZVV0c5yWE.width-800.png)

Expand the _Subscribe to bot events_ section and click the **Add Bot User Event** button. Select the **app\_mention** event to subscribe, then save your changes.

![list of bot events to subscribe to](https://twilio-cms-prod.s3.amazonaws.com/images/lBENb6c707lxNqaBYFzSeoGQdGyPvb48__cgmIcqVfS5qS.width-800.png)

### Install the Slackbot to a workspace

Once you have configured your bot, we can install it to a workspace. To do this, navigate to “Install App” under _Settings_.

![request to install app to the corporate team workspace](https://twilio-cms-prod.s3.amazonaws.com/images/Vc4yeEoRxl_IwfXK4f1pGJpCSn_ksi2EBSTaFYvf61L7e0.width-800.png)

If your workspace is restricted, like Twilio’s and many large organizations, you will need to request to install your app before you can view it in your workspace. This process might look different depending on your organization. At Twilio, after you request to install an app, you will be required to submit a request via the Service Now portal to approve your app.

There might be other cases where you will need to create a request when working from behind a corporate firewall. For example, you might want to use [Webhooks](https://api.slack.com/messaging/webhooks) to gather real time data for your app. Webhooks require a URL to send JSON data to. You might be able to request a unique URL from your organization for this purpose.

Once your app has been approved, revisit the _Install App_ tab. You should now have access to a Bot User OAuth Token. ![OAuth tokens for the workspace](https://twilio-cms-prod.s3.amazonaws.com/images/naOzjoEpyUbV40bFvqXsZTheY_06UiYJ7wALB--10DJOOu.width-800.png)

This is your `SLACK_BOT_TOKEN`. A Slackbot token references a bot user instance of the app you have created, and specifically sets the scopes that your bot has access to.

## Run from source
### Virtual Enviroment (Optical,Recommended)
```bash
pip install virtualenv
virtualenv env
source env/bin/activate
pip install -r requirements.txt
```

`echo "SLACK_BOT_TOKEN='xoxb-****'" > .env`

`echo "SLACK_APP_TOKEN='xapp-****'" >> .env`

```bash
python bot.py
```

## Docker Usage

### Build Source
```bash
docker build -t slack-bot .
docker run -it --name bravetalk-slack --restart always -e SLACK_BOT_TOKEN='xoxb-****' -e SLACK_APP_TOKEN='xapp-****' slack-bot
```

### From Public Registry
```bash
docker run -it --name bravetalk-slack --restart always -e SLACK_BOT_TOKEN='xoxb-****' -e SLACK_APP_TOKEN='xapp-****' registry.gitlab.com/bravetalk/slack-bot
```