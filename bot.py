import os
import logging
from dotenv import load_dotenv

from slack_bolt import App
from slack_bolt.adapter.socket_mode import SocketModeHandler

from genslug import check_url

load_dotenv()

token = os.environ.get('SLACK_BOT_TOKEN')
app = App(token=token)

@app.message("create-meeting")
def message_hello(message, say):
    say(f"<@{message['user']}> \n Here is your MeetingLink: {check_url()}")

@app.event("message")
def handle_message_events(body, logger):
    logger.info(body)

if __name__ == "__main__":
    SocketModeHandler(app, os.environ["SLACK_APP_TOKEN"]).start()